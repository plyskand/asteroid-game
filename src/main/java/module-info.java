module ankach.asteroidgame {
    requires javafx.controls;
    requires javafx.fxml;


    opens ankach.asteroidgame to javafx.fxml;
    exports ankach.asteroidgame;
}