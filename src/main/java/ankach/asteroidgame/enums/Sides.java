package ankach.asteroidgame.enums;

public class Sides {
    public final static String SIDE_PLAYER = "PLAYER";
    public final static String SIDE_ASTEROIDS = "ASTEROIDS";
    public final static String SIDE_ENEMY = "ENEMY";
}
