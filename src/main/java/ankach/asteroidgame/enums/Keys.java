package ankach.asteroidgame.enums;

import javafx.scene.input.KeyCode;

public class Keys {
    public final static String KEY_FIRE = KeyCode.SPACE.toString();
    public final static String KEY_ROTATE_LEFT = KeyCode.Z.toString();
    public final static String KEY_ROTATE_RIGHT = KeyCode.X.toString();
    public final static String KEY_MOVE_STRAIGHT = KeyCode.UP.toString();
    public final static String KEY_MOVE_BACK = KeyCode.DOWN.toString();
    public final static String KEY_MOVE_LEFT = KeyCode.LEFT.toString();
    public final static String KEY_MOVE_RIGHT = KeyCode.RIGHT.toString();
    public final static String KEY_SHIFT = KeyCode.SHIFT.toString();
}
