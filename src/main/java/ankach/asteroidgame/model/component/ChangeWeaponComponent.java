package ankach.asteroidgame.model.component;

import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.model.attribute.Attribute;
import ankach.asteroidgame.model.attribute.WeaponAttribute;

public class ChangeWeaponComponent extends Component {
    public ChangeWeaponComponent() {
        super(Component.NAME_CHANGE_WEAPON);
    }

    public void changeWeapon(Entity e) {
        ((WeaponAttribute) e.getAttribute(Attribute.T_WEAPON)).changeWeapon();
    }
}
