package ankach.asteroidgame.model.component;

import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.model.attribute.Attribute;
import ankach.asteroidgame.model.attribute.RotationAttribute;

public class RotationComponent extends Component{
    public RotationComponent(String name) {
        super(name);
    }

    public void rotateLeft(Entity e) {
        RotationAttribute attribute = (RotationAttribute) e.getAttribute(Attribute.T_ROTATION);
        rotate(e, -attribute.getSpeed());
    }

    public void rotateRight(Entity e) {
        RotationAttribute attribute = (RotationAttribute) e.getAttribute(Attribute.T_ROTATION);
        rotate(e, attribute.getSpeed());
    }

    private void rotate(Entity e, double speed) {
        e.setPreviousAngle(e.getAngle());
        e.addAngle(speed);
    }
}
