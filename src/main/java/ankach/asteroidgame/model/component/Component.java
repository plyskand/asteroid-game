package ankach.asteroidgame.model.component;

import ankach.asteroidgame.controller.command.Command;
import ankach.asteroidgame.entity.Entity;

public abstract class Component {
    public final static String NAME_ONE_DIRECTION_MOVING = "One-direction-moving";
    public final static String NAME_MOVING = "Moving";
    public final static String NAME_ROTATION = "Rotation";
    public final static String NAME_RENDER = "Render";
    public final static String NAME_RENDER_SHIP = "RENDER_SHIP";
    public final static String NAME_FIRE = "FIRE";
    public final static String NAME_CHANGE_WEAPON = "CHANGE_WEAPON";
    public final static String NAME_EXPLOSION_RENDER = "EXPLOSION_RENDER";

    private final String name;

    public Component(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean makeAction(Command command, Entity entity) {
        return command.execute(this, entity);
    }
}
