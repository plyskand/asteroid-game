package ankach.asteroidgame.model.component;

import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.model.attribute.Attribute;
import ankach.asteroidgame.model.attribute.DirectionAttribute;

public class OneDirectionMovingComponent extends MovingComponent {
    public OneDirectionMovingComponent(String name) {
        super(name);
    }

    @Override
    public void moveStraight(Entity entity) {
        DirectionAttribute direction = (DirectionAttribute) entity.getAttribute(Attribute.T_DIRECTION);
        this.setNewPosition(entity, direction.getDirection(), getSpeed(entity, Attribute.T_SPEED));
    }
}
