package ankach.asteroidgame.model.component;

import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.model.attribute.Attribute;
import ankach.asteroidgame.model.attribute.HPAttribute;
import ankach.asteroidgame.render.Render;
import javafx.scene.paint.Color;

public class ShipRenderComponent extends RenderComponent{

    public ShipRenderComponent(String name) {
        super(name);
    }

    @Override
    public void render(Entity e, Render render) {
        super.render(e, render);
        HPAttribute a = (HPAttribute) e.getAttribute(Attribute.T_HP);
        render.drawHp(
                a.getArmor(),
                a.getMaxArmor(),
                e.getXPos(),
                e.getYPos() - 15,
                e.getSprite().getImage().getWidth(),
                Color.LIME,
                Color.GREEN
        );
        render.drawHp(
                a.getShield(),
                a.getMaxShield(),
                e.getXPos(),
                e.getYPos() - 25,
                e.getSprite().getImage().getWidth(),
                Color.BLUE,
                Color.LIGHTBLUE
        );
    }
}
