package ankach.asteroidgame.model.component;

import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.render.Render;

public class RenderComponent extends Component {
    public RenderComponent(String name) {
        super(name);
    }

    public void render(Entity e, Render render) {
        render.drawRotatedEntity(e);
    }
}
