package ankach.asteroidgame.model.component;

import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.render.Render;

public class ExplosionRenderComponent extends RenderComponent {
    public ExplosionRenderComponent(String name) {
        super(name);
    }

    public void render(Entity e, Render render) {
        render.drawScaledEntity(e);
    }
}
