package ankach.asteroidgame.model.component;

import ankach.asteroidgame.entity.Bullet;
import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.entity.Sprite;
import ankach.asteroidgame.entity.Weapon;
import ankach.asteroidgame.model.SceneManager;
import ankach.asteroidgame.model.attribute.Attribute;
import ankach.asteroidgame.model.attribute.DirectionAttribute;
import ankach.asteroidgame.model.attribute.WeaponAttribute;
import ankach.asteroidgame.spawner.BulletSpawner;

import java.nio.channels.spi.SelectorProvider;

public class FireComponent extends Component {
    private final SceneManager manager;
    private final BulletSpawner bulletSpawner;

    public FireComponent(String name) {
        super(name);
        this.manager = SceneManager.getManager();
        this.bulletSpawner = new BulletSpawner();
    }

    public void fire(Entity e) {
        WeaponAttribute attribute = (WeaponAttribute) e.getAttribute(Attribute.T_WEAPON);

        int countPerShot = attribute.getCountPerShot();
        Weapon weapon = attribute.getWeapon();
        if (!weapon.canFire()) return;

        for (int i = 0; i < countPerShot; i++) {
            Bullet bullet = bulletSpawner.createBullet(weapon);
            Sprite sprite = bullet.getSprite();
            int bulletWidth = sprite.getWidth();
            int bulletHeight = sprite.getHeight();
            double direction = e.getAngle();
            double chdir = getChangedDirection(i);
            bullet.setAngle(direction + 90 + chdir);
            bullet.setPos(e.getCenterXPos() - bulletWidth/2.0, e.getCenterYPos() - bulletHeight/2.0);
            bullet.addAttribute(new DirectionAttribute(direction + chdir));
            manager.addBullet(bullet);
        }
    }

    private double getChangedDirection(int i) {
        if (i % 2 == 0) {
            return (i - 1) * 2;
        }
        return i*-2;
    }
}
