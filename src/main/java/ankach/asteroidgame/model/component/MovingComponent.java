package ankach.asteroidgame.model.component;

import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.model.attribute.Attribute;
import ankach.asteroidgame.model.attribute.SpeedAttribute;

/**
 * Class is responsible for moving game objects.
 * It is part of ACS pattern
 * */
public class MovingComponent extends Component {
    public MovingComponent(String name) {
        super(name);
    }

    protected void setNewPosition(Entity entity, double direction, double speed) {
        double y = entity.getYPos() + speed * Math.sin(Math.toRadians(direction));
        double x = entity.getXPos() + speed * Math.cos(Math.toRadians(direction));

        entity.setPreviousXPos(entity.getXPos());
        entity.setPreviousYPos(entity.getYPos());
        entity.setXPos(x);
        entity.setYPos(y);
    }

    protected int getSpeed(Entity entity, String attrName) {
        Attribute attribute = entity.getAttribute(attrName);
        if (attribute == null) return 0;

        return ((SpeedAttribute) attribute).getSpeed();
    }

    public void moveStraight(Entity entity) {
        setNewPosition(entity, entity.getAngle(), getSpeed(entity, Attribute.T_SPEED));
    }

    public void moveBack(Entity entity) {
        setNewPosition(entity, entity.getAngle() - 180, getSpeed(entity, Attribute.T_SPEED_JINK));
    }

    public void moveRight(Entity entity) {
        setNewPosition(entity, entity.getAngle() + 90, getSpeed(entity, Attribute.T_SPEED_JINK));
    }

    public void moveLeft(Entity entity) {
        setNewPosition(entity, entity.getAngle() - 90, getSpeed(entity, Attribute.T_SPEED_JINK));
    }
}
