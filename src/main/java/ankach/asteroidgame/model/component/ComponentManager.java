package ankach.asteroidgame.model.component;

import ankach.asteroidgame.builder.ComponentManagerBuilder;

import java.util.Map;

public class ComponentManager {
    private final Map<String, Component> components;

    public static ComponentManager manager;

    public ComponentManager(Map<String, Component> components) {
        this.components = components;
    }

    public Component getComponent(String name) {
        return components.get(name);
    }

    public static ComponentManager getManager() {
        if (manager == null) {
            ComponentManagerBuilder builder = new ComponentManagerBuilder();
            manager = builder.addComponent(Component.NAME_MOVING, new MovingComponent(Component.NAME_MOVING))
                    .addComponent(Component.NAME_CHANGE_WEAPON, new ChangeWeaponComponent())
                    .addComponent(Component.NAME_ROTATION, new RotationComponent(Component.NAME_ROTATION))
                    .addComponent(Component.NAME_ONE_DIRECTION_MOVING, new OneDirectionMovingComponent(Component.NAME_MOVING))
                    .addComponent(Component.NAME_RENDER, new RenderComponent(Component.NAME_RENDER))
                    .addComponent(Component.NAME_RENDER_SHIP, new ShipRenderComponent(Component.NAME_RENDER))
                    .addComponent(Component.NAME_EXPLOSION_RENDER, new ExplosionRenderComponent(Component.NAME_RENDER))
                    .addComponent(Component.NAME_FIRE, new FireComponent(Component.NAME_FIRE))
                    .build();
        }
        return manager;
    }

    public static void reset() {
        manager = null;
    }
}
