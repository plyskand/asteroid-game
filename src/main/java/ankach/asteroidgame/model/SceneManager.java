package ankach.asteroidgame.model;

import ankach.asteroidgame.entity.*;
import ankach.asteroidgame.model.attribute.Attribute;
import ankach.asteroidgame.model.attribute.SizeAttribute;

import java.util.ArrayList;
import java.util.List;

public class SceneManager {
    private final InfoPanel panel;
    private Sprite background;
    private Ship ship;
    private final Score score;
    boolean gameOver = false;

    private final List<Entity> asteroids = new ArrayList<>();
    private final List<Entity> bullets = new ArrayList<>();
    private final List<Entity> explosions = new ArrayList<>();

    private static SceneManager manager;

    public SceneManager() {
        panel = new InfoPanel();
        score = new Score();
    }

    public Ship getShip() {
        return ship;
    }

    public void setShip(Ship ship) {
        this.ship = ship;
    }

    public List<Entity> getAsteroids() {
        return asteroids;
    }

    public void addAsteroid(Asteroid asteroid) {
        asteroids.add(asteroid);
    }

    public Sprite getBackground() {
        return background;
    }

    public void setBackground(Sprite background) {
        this.background = background;
    }

    public void removeEntity(Entity e) {
        if (e.equals(ship)) gameOver = true;
        if (e instanceof Asteroid) {
            asteroids.remove(e);
            SizeAttribute size = (SizeAttribute) e.getAttribute(Attribute.T_SIZE);
            score.increaseScore((int) (size.getSize()/10.0));
            return;
        }

        bullets.remove(e);
    }

    public List<Entity> getAllEntities() {
        List<Entity> copy = new ArrayList<>(asteroids);
        copy.add(ship);
        return copy;
    }

    public List<Entity> getBullets() {
        return bullets;
    }

    public void addBullet(Bullet bullet) {
        bullets.add(bullet);
    }

    public void addExplosion(Explosion e) {
        explosions.add(e);
    }

    public void removeExplosion(Explosion e) {
        explosions.remove(e);
    }

    public List<Entity> getExplosions() {
        return explosions;
    }

    public boolean isGameOver() {
        return gameOver;
    }

    public InfoPanel getPanel() {
        return panel;
    }

    public Score getScore() {
        return score;
    }

    public int getAmountOfKilledAsteroids() {
        return score.getAmountOfKilledAsteroids();
    }

    public static SceneManager getManager() {
        if (manager == null) {
            manager = new SceneManager();
        }
        return manager;
    }

    public static void reset() {
        manager = new SceneManager();
    }
}
