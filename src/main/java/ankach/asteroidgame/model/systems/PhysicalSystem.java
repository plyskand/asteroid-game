package ankach.asteroidgame.model.systems;

import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.model.attribute.Attribute;
import ankach.asteroidgame.model.attribute.DamageAttribute;
import ankach.asteroidgame.model.attribute.HPAttribute;
import ankach.asteroidgame.model.messaging.*;

import java.util.List;

public class PhysicalSystem extends ISystem {

    public PhysicalSystem(MessageBus bus) {
        super(bus, MessageType.M_PHYSIC_SYSTEM);
        bus.subscribe(massageType);
    }

    @Override
    public void update() {
        List<Message> messages = bus.receive(massageType);
        messages.forEach((message -> {
            PhysicMessage pm = (PhysicMessage) message;
            Entity attacker = pm.getFirst();
            Entity defender = pm.getSecond();
            HPAttribute hpAttacker = (HPAttribute) attacker.getAttribute(Attribute.T_HP);
            HPAttribute hpDefender = (HPAttribute) defender.getAttribute(Attribute.T_HP);
            DamageAttribute dmAttacker = (DamageAttribute) attacker.getAttribute(Attribute.T_DAMAGE);
            DamageAttribute dmDefender = (DamageAttribute) defender.getAttribute(Attribute.T_DAMAGE);
            int damageA = dmAttacker.getDamage();
            int damageD = dmDefender.getDamage();
            hpAttacker.getDamage(damageD);
            hpDefender.getDamage(damageA);
            if (hpAttacker.isDead()) {
                this.bus.send(new EntityMessage(MessageType.M_DESTROY_SYSTEM, attacker));
            }
            if (hpDefender.isDead()) {
                this.bus.send(new EntityMessage(MessageType.M_DESTROY_SYSTEM, defender));
            }
       }));
    }
}
