package ankach.asteroidgame.model.systems;

import ankach.asteroidgame.controller.command.Command;
import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.model.component.Component;
import ankach.asteroidgame.model.messaging.CommandMessage;
import ankach.asteroidgame.model.messaging.Message;
import ankach.asteroidgame.model.messaging.MessageBus;
import ankach.asteroidgame.model.messaging.MessageType;

import java.util.List;

public class FireSystem extends ISystem {
    public FireSystem(MessageBus bus) {
        super(bus, MessageType.M_FIRE_SYSTEM);
        bus.subscribe(massageType);
    }

    @Override
    public void update() {
        List<Message> messages = bus.receive(massageType);
        messages.forEach(m -> {
            CommandMessage cm = (CommandMessage) m;
            Entity entity = cm.getEntity();
            if (entity == null) return;
            Component fire = entity.getComponent(Component.NAME_FIRE);
            Component change = entity.getComponent(Component.NAME_CHANGE_WEAPON);
            Command c = cm.getCommand();
            fire.makeAction(c, entity);
            change.makeAction(c, entity);
        });
    }
}
