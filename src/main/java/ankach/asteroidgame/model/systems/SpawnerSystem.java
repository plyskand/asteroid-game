package ankach.asteroidgame.model.systems;

import ankach.asteroidgame.entity.Asteroid;
import ankach.asteroidgame.model.SceneManager;
import ankach.asteroidgame.model.messaging.MessageBus;
import ankach.asteroidgame.model.messaging.MessageType;
import ankach.asteroidgame.spawner.AsteroidSpawner;

import java.util.List;

public class SpawnerSystem extends ISystem {

    private final SceneManager manager;
    private final AsteroidSpawner asteroidSpawner;

    public SpawnerSystem(MessageBus bus, AsteroidSpawner spawner) {
        super(bus, MessageType.M_SPAWNER_SYSTEM);
        bus.subscribe(this.massageType);
        this.manager = SceneManager.getManager();
        this.asteroidSpawner = spawner;
    }

    @Override
    public void update() {
        Asteroid a = asteroidSpawner.createAsteroid(manager.getAsteroids().size());
        if (a == null) return;
        manager.addAsteroid(a);
    }
}
