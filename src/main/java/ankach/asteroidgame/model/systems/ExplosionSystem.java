package ankach.asteroidgame.model.systems;

import ankach.asteroidgame.Config;
import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.entity.Explosion;
import ankach.asteroidgame.entity.Sprite;
import ankach.asteroidgame.model.SceneManager;
import ankach.asteroidgame.model.component.Component;
import ankach.asteroidgame.model.component.ComponentManager;
import ankach.asteroidgame.model.messaging.EntityMessage;
import ankach.asteroidgame.model.messaging.MessageBus;
import ankach.asteroidgame.model.messaging.MessageType;
import ankach.asteroidgame.resources.ResourceManager;

import java.util.Iterator;
import java.util.List;

public class ExplosionSystem extends ISystem {
    private final SceneManager manager;
    private final ResourceManager resourceManager;

    public ExplosionSystem(MessageBus bus) {
        super(bus, MessageType.M_EXPLOSION_SYSTEM);
        bus.subscribe(massageType);
        manager = SceneManager.getManager();
        resourceManager = ResourceManager.getManager();
    }

    @Override
    public void update() {
        Iterator<Entity> explosionsIterator = manager.getExplosions().iterator();
        while(explosionsIterator.hasNext()) {
            Explosion explosion = (Explosion) explosionsIterator.next();
            if (!explosion.isAlive()) explosionsIterator.remove();
        }

        this.bus.receive(massageType).forEach((m) -> {
            EntityMessage em = (EntityMessage) m;
            Entity entity = em.getEntity();
            Explosion explosion = create(entity);
            manager.addExplosion(explosion);
        });
    }

    private Explosion create(Entity e) {
        List<String> explosions = Config.getExplosions();
        Sprite entitySprite = e.getSprite();
        int width = entitySprite.getWidth();
        int height = entitySprite.getHeight();

        Sprite sprite = resourceManager.createSprite(width, height, explosions.get(0));
        Explosion explosion = new Explosion(sprite, 30);
        explosion.setPos(e.getXPos(), e.getYPos());
        explosion.addComponent(ComponentManager.getManager().getComponent(Component.NAME_EXPLOSION_RENDER));
        for (int i = 1; i <= 8; ++i) {
            explosion.addSprite(resourceManager.createSprite(width, height, explosions.get(i)));
        }
        return explosion;
    }
}
