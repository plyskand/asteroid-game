package ankach.asteroidgame.model.systems;

import ankach.asteroidgame.model.messaging.MessageBus;

public abstract class ISystem {
    protected MessageBus bus;
    protected String massageType;

    public ISystem(MessageBus bus, String massageType) {
        this.massageType = massageType;
        this.bus = bus;
    }

    public abstract void update();
}
