package ankach.asteroidgame.model.systems;

import ankach.asteroidgame.controller.InputController;
import ankach.asteroidgame.controller.command.Command;
import ankach.asteroidgame.enums.Keys;
import ankach.asteroidgame.model.SceneManager;
import ankach.asteroidgame.model.messaging.CommandMessage;
import ankach.asteroidgame.model.messaging.MessageBus;
import ankach.asteroidgame.model.messaging.MessageType;

import java.util.List;

public class InputSystem extends ISystem {
    private final InputController controller;
    private final SceneManager sceneManager;

    public InputSystem(MessageBus bus, InputController controller) {
        super(bus, MessageType.M_INPUT_SYSTEM);
        bus.subscribe(this.massageType);
        this.controller = controller;
        this.sceneManager = SceneManager.getManager();
    }

    @Override
    public void update() {
        List<Command> commands = controller.getCommands();
        commands.forEach((c) -> {
            String id = c.getIdentifier();
            if (id.compareTo(Keys.KEY_FIRE) == 0 || id.compareTo(Keys.KEY_SHIFT) == 0) {
                bus.send(new CommandMessage(MessageType.M_FIRE_SYSTEM, c,  sceneManager.getShip()));
            } else if (id.compareTo(Keys.KEY_ROTATE_LEFT) == 0 || id.compareTo(Keys.KEY_ROTATE_RIGHT) == 0) {
                bus.send(new CommandMessage(MessageType.M_ROTATE_SYSTEM, c, sceneManager.getShip()));
            } else {
                bus.send(new CommandMessage(MessageType.M_MOVING_SYSTEM, c, sceneManager.getShip()));
            }
        });
    }
}
