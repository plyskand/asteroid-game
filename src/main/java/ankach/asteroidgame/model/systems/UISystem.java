package ankach.asteroidgame.model.systems;

import ankach.asteroidgame.controller.command.MoveStraightCommand;
import ankach.asteroidgame.controller.command.RotateRightCommand;
import ankach.asteroidgame.model.SceneManager;
import ankach.asteroidgame.model.messaging.CommandMessage;
import ankach.asteroidgame.model.messaging.Message;
import ankach.asteroidgame.model.messaging.MessageBus;
import ankach.asteroidgame.model.messaging.MessageType;

import java.util.List;

public class UISystem extends ISystem {

    private final SceneManager manager;

    public UISystem(MessageBus bus) {
        super(bus, MessageType.M_UI_SYSTEM);
        bus.subscribe(this.massageType);
        this.manager = SceneManager.getManager();
    }

    @Override
    public void update() {
        manager.getAsteroids().forEach(a -> {
            bus.send(new CommandMessage(MessageType.M_MOVING_SYSTEM, new MoveStraightCommand(), a));
            bus.send(new CommandMessage(MessageType.M_ROTATE_SYSTEM, new RotateRightCommand(), a));
        });

        manager.getBullets().forEach(b -> {
            bus.send(new CommandMessage(MessageType.M_MOVING_SYSTEM, new MoveStraightCommand(), b));
        });
    }
}
