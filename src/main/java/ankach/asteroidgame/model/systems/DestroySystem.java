package ankach.asteroidgame.model.systems;

import ankach.asteroidgame.entity.Asteroid;
import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.model.SceneManager;
import ankach.asteroidgame.model.messaging.EntityMessage;
import ankach.asteroidgame.model.messaging.Message;
import ankach.asteroidgame.model.messaging.MessageBus;
import ankach.asteroidgame.model.messaging.MessageType;

import java.util.List;

public class DestroySystem extends ISystem {
    private final SceneManager manager;

    public DestroySystem(MessageBus bus) {
        super(bus, MessageType.M_DESTROY_SYSTEM);
        bus.subscribe(this.massageType);
        this.manager = SceneManager.getManager();
    }

    @Override
    public void update() {
       List<Message> messages = bus.receive(massageType);
       messages.forEach((m) -> {
           EntityMessage em = (EntityMessage) m;
           Entity entity = em.getEntity();
           if (entity instanceof Asteroid) bus.send(new EntityMessage(MessageType.M_EXPLOSION_SYSTEM, entity));
           manager.removeEntity(entity);
       });
    }
}
