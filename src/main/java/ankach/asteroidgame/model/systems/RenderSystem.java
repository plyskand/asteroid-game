package ankach.asteroidgame.model.systems;

import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.entity.InfoPanel;
import ankach.asteroidgame.entity.Ship;
import ankach.asteroidgame.model.SceneManager;
import ankach.asteroidgame.model.attribute.Attribute;
import ankach.asteroidgame.model.attribute.HPAttribute;
import ankach.asteroidgame.model.attribute.SpeedAttribute;
import ankach.asteroidgame.model.attribute.WeaponAttribute;
import ankach.asteroidgame.model.component.Component;
import ankach.asteroidgame.model.component.RenderComponent;
import ankach.asteroidgame.model.messaging.MessageBus;
import ankach.asteroidgame.model.messaging.MessageType;
import ankach.asteroidgame.render.Render;
import ankach.asteroidgame.resources.ResourceManager;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.util.List;

public class RenderSystem extends ISystem {
    private final Render render;
    private final SceneManager sceneManager;
    private final ResourceManager resourceManager;

    public RenderSystem(MessageBus bus, Render render) {
        super(bus, MessageType.M_RENDER_SYSTEM);
        this.render = render;
        this.sceneManager = SceneManager.getManager();
        this.resourceManager = ResourceManager.getManager();
        init();
    }

    private void init() {
        Ship ship = sceneManager.getShip();
        resourceManager.loadImage(ship.getSprite().getPath(), "panel",60, 60);
        WeaponAttribute weaponAttribute = (WeaponAttribute) ship.getAttribute(Attribute.T_WEAPON);
        weaponAttribute.getWeapons().forEach((w) -> {
           resourceManager.loadImage(w.getPath(), "panel", 60, 60);
        });
    }

    @Override
    public void update() {
        render.clear();
        render(sceneManager.getBullets());
        render(sceneManager.getAllEntities());
        render(sceneManager.getExplosions());
        renderPanel(sceneManager.getPanel(), sceneManager.getShip());
    }

    public void render(List<Entity> entities) {
        entities.forEach((e) -> {
            RenderComponent c = (RenderComponent) e.getComponent(Component.NAME_RENDER);
            c.render(e, render);
        });
    }

    public void renderPanel(InfoPanel panel, Ship ship) {
        Rectangle2D infoPanel = panel.getPanel();
        Rectangle2D shipPanel = panel.getShipPanel();
        Rectangle2D weaponPanel = panel.getWeaponPanel();
        Rectangle2D scorePanel = panel.getScorePanel();

        WeaponAttribute gun = (WeaponAttribute) ship.getAttribute(Attribute.T_WEAPON);
        render.drawRect(infoPanel.getMinX(), infoPanel.getMinY(), infoPanel.getWidth(), infoPanel.getHeight(), Color.DARKBLUE);
        render.drawRect(shipPanel.getMinX(), shipPanel.getMinY(), shipPanel.getWidth(), shipPanel.getHeight(), Color.BLUE);
        render.drawRect(weaponPanel.getMinX(), weaponPanel.getMinY(), weaponPanel.getWidth(), weaponPanel.getHeight(), Color.BLUE);
        render.drawRect(scorePanel.getMinX(), scorePanel.getMinY(), scorePanel.getWidth(), scorePanel.getHeight(), Color.BLUE);

        this.renderShipPanel(panel, shipPanel, ship);
        this.renderWeaponPanel(panel, weaponPanel, gun);
        this.renderScorePanel(panel, scorePanel);
    }

    private void renderShipPanel(InfoPanel panel, Rectangle2D shipPanel, Ship ship) {
        HPAttribute hp = (HPAttribute) ship.getAttribute(Attribute.T_HP);
        SpeedAttribute speed = (SpeedAttribute) ship.getAttribute(Attribute.T_SPEED);

        render.drawText(shipPanel.getMinX() + shipPanel.getWidth()/6, shipPanel.getMinY() + 30,panel.getShipText(), Color.WHITE, 25);
        Image image = resourceManager.getImageById(ship.getSprite().getPath() + "panel");
        render.drawImage(image, shipPanel.getMinX() + shipPanel.getWidth()/2 - image.getWidth()/2, shipPanel.getMinY() + 30, -90);
        render.drawText(shipPanel.getMinX() + 20, shipPanel.getMinY() + 110 , panel.getArmorText() + hp.getArmor(),  Color.WHITE, 20);
        render.drawText(shipPanel.getMinX() + 20, shipPanel.getMinY() + 135 , panel.getShieldText() + hp.getShield(),  Color.WHITE, 20);
        render.drawText(shipPanel.getMinX() + 20, shipPanel.getMinY() + 160 , panel.getSpeedText() + speed.getSpeed(),  Color.WHITE, 20);
    }

    public void renderWeaponPanel(InfoPanel panel, Rectangle2D weaponPanel, WeaponAttribute gun) {
        render.drawText(weaponPanel.getMinX() + 5, weaponPanel.getMinY() + 30, panel.getWeaponText(), Color.WHITE, 25);
        Image image = resourceManager.getImageById(gun.getWeapon().getPath() + "panel");
        render.drawImage(image, weaponPanel.getMinX() + weaponPanel.getWidth()/2 - image.getWidth()/2, weaponPanel.getMinY() + 30, 0);
        render.drawText(weaponPanel.getMinX() + 20, weaponPanel.getMinY() + 110, panel.getAttackText() + gun.getWeaponAttack(),  Color.WHITE, 20);
        render.drawText(weaponPanel.getMinX() + 20, weaponPanel.getMinY() + 135, panel.getCountText() + gun.getChosenCount(),  Color.WHITE, 20);
        render.drawText(weaponPanel.getMinX() + 20, weaponPanel.getMinY() + 160 , panel.getSpeedText() + gun.getSpeed(),  Color.WHITE, 20);
        render.drawText(weaponPanel.getMinX() + 20, weaponPanel.getMinY() + 185 , "Reload in " + gun.getDelay(),  Color.WHITE, 20);
    }

    public void renderScorePanel(InfoPanel panel, Rectangle2D scorePanel) {
        render.drawText(scorePanel.getMinX() + scorePanel.getWidth()/4 + 10, scorePanel.getMinY() + 30, panel.getScoreText(), Color.WHITE, 25);
        render.drawText(scorePanel.getMinX() + 20, scorePanel.getMinY() + 60, panel.getScoreText()+ ": " +sceneManager.getScore().getScore() , Color.WHITE, 20);
        render.drawText(scorePanel.getMinX() + 20, scorePanel.getMinY() + 90, "Amount of destroyed asteroids: " +sceneManager.getAmountOfKilledAsteroids() , Color.WHITE, 20);
    }
}
