package ankach.asteroidgame.model.systems;

import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.model.component.Component;
import ankach.asteroidgame.model.messaging.CommandMessage;
import ankach.asteroidgame.model.messaging.MessageBus;
import ankach.asteroidgame.model.messaging.MessageType;

public class RotateSystem extends ISystem{
    public RotateSystem(MessageBus bus) {
        super(bus, MessageType.M_ROTATE_SYSTEM);
        bus.subscribe(this.massageType);
    }

    @Override
    public void update() {
        bus.receive(this.massageType).forEach((c) -> {
            CommandMessage cm = (CommandMessage) c;
            Entity entity = cm.getEntity();
            if (entity == null) return;
            Component component = entity.getComponent(Component.NAME_ROTATION);
            component.makeAction(cm.getCommand(), entity);
        });
    }
}
