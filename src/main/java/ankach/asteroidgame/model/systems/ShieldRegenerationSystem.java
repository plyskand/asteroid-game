package ankach.asteroidgame.model.systems;

import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.model.SceneManager;
import ankach.asteroidgame.model.attribute.Attribute;
import ankach.asteroidgame.model.attribute.HPAttribute;
import ankach.asteroidgame.model.messaging.MessageBus;
import ankach.asteroidgame.model.messaging.MessageType;

public class ShieldRegenerationSystem extends ISystem{
    private final SceneManager manager;

    public ShieldRegenerationSystem(MessageBus bus) {
        super(bus, MessageType.M_SHIELD_SYSTEM);
        this.manager = SceneManager.getManager();
        bus.subscribe(this.massageType);
    }

    @Override
    public void update() {
        Entity ship = manager.getShip();
        HPAttribute hp = (HPAttribute) ship.getAttribute(Attribute.T_HP);
        hp.regenerateShield();
    }
}
