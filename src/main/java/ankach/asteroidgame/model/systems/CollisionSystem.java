package ankach.asteroidgame.model.systems;

import ankach.asteroidgame.Config;
import ankach.asteroidgame.entity.Bullet;
import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.model.SceneManager;
import ankach.asteroidgame.model.attribute.Attribute;
import ankach.asteroidgame.model.attribute.SideAttribute;
import ankach.asteroidgame.model.messaging.*;
import javafx.geometry.Point2D;

import java.util.List;

public class CollisionSystem extends ISystem {

    private final SceneManager manager;

    public CollisionSystem(MessageBus bus) {
        super(bus, MessageType.M_COLLISION_SYSTEM);
        this.manager = SceneManager.getManager();
        bus.subscribe(this.massageType);
    }

    @Override
    public void update() {
        Entity ship = manager.getShip();
        SideAttribute shipSide = (SideAttribute) ship.getAttribute(Attribute.T_SIDE);

        bus.receive(this.massageType).forEach((m) -> {
            EntityMessage em = (EntityMessage) m;
            Entity curr = em.getEntity();
            if (curr == null) return;
            correctEntityPath(curr);
            SideAttribute currSide = (SideAttribute) curr.getAttribute(Attribute.T_SIDE);
            if (curr != ship) {                          // same object => continue
                if (currSide.getSide().compareTo(shipSide.getSide()) == 0)
                    return;          // same player side => continue
                if (intersect(curr, ship)) {
                    curr.restorePreviousPos();
                    bus.send(new PhysicMessage(MessageType.M_PHYSIC_SYSTEM, curr, ship));
                }
            }

            this.processCollisionsWithObjects(curr, currSide, manager.getAsteroids());
            this.processCollisionsWithObjects(curr, currSide, manager.getBullets());
        });
    }

    public void processCollisionsWithObjects(Entity curr, SideAttribute currSide, List<Entity> entities) {
        entities.forEach((e) -> {
            if (curr != e && intersect(curr, e)){
                SideAttribute bSide = (SideAttribute) e.getAttribute(Attribute.T_SIDE);
                if (currSide.getSide().compareTo(bSide.getSide()) != 0) {
                    curr.restorePreviousPos();
                    bus.send(new PhysicMessage(MessageType.M_PHYSIC_SYSTEM, curr, e));
                }
            }
        });
    }

    private boolean intersect(Entity e1, Entity e2) {
        double distance = new Point2D(e1.getCenterXPos(), e1.getCenterYPos()).distance(e2.getCenterXPos(), e2.getCenterYPos());
        return distance <= e1.getCircleRadius() + e2.getCircleRadius() - 20;
    }

    private void correctEntityPath(Entity e) { //todo
        double x = e.getXPos();
        double y = e.getYPos();
        double windowHeight = Config.WINDOW_HEIGHT;
        double windowWidth = Config.WINDOW_WIDTH;

        if (e instanceof Bullet) {
            if (x > windowWidth || y > windowHeight || x < -50 || y < -50)
                bus.send(new EntityMessage(MessageType.M_DESTROY_SYSTEM, e));
            return;
        }
        double xp = x;
        double yp = y;


        double panelHeight = Config.PANEL_HEIGHT;
        double panelWidth = Config.PANEL_WIDTH;
        double border = 50;

        if (x > windowWidth + border) x = -border;
        if (x < - border) x = windowWidth;
        if (y > windowHeight) y = -border;
        if (y < - border) y = windowHeight;

        e.setPos(x, y);
        e.setPreviousYPos(xp);
        e.setPreviousYPos(yp);
    }
}
