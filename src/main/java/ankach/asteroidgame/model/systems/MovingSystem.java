package ankach.asteroidgame.model.systems;

import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.model.component.Component;
import ankach.asteroidgame.model.messaging.*;

public class MovingSystem extends ISystem {
    public MovingSystem(MessageBus bus) {
        super(bus, MessageType.M_MOVING_SYSTEM);
        bus.subscribe(this.massageType);
    }

    @Override
    public void update() {
        bus.receive(this.massageType).forEach((m) -> {
            CommandMessage cm = (CommandMessage) m;
            Entity entity = cm.getEntity();
            if (entity == null) return;
            Component component = entity.getComponent(Component.NAME_MOVING);
            if (component.makeAction(cm.getCommand(), entity)) {
                bus.send(new EntityMessage(MessageType.M_COLLISION_SYSTEM, entity));
            }
        });
    }
}
