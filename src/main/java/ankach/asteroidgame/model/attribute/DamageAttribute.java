package ankach.asteroidgame.model.attribute;

public class DamageAttribute extends Attribute {
    private int damage;

    public DamageAttribute(int damage) {
        super(Attribute.T_DAMAGE);
        this.damage = damage;
    }

    public int getDamage() {
        return damage;
    }
}
