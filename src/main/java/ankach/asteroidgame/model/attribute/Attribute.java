package ankach.asteroidgame.model.attribute;

public abstract class Attribute {
    public final static String T_ROTATION = "ROTATION";
    public final static String T_SPEED = "SPEED";
    public final static String T_DIRECTION = "DIRECTION";
    public final static String T_SPEED_JINK = "SPEED_JINK";
    public final static String T_WEAPON = "WEAPON";
    public final static String T_SIZE = "SIZE";
    public final static String T_SIDE = "SIDE";
    public final static String T_DAMAGE = "DAMAGE";
    public final static String T_HP = "HP";

    public String name;

    public Attribute(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
