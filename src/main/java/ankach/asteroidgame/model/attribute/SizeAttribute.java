package ankach.asteroidgame.model.attribute;

public class SizeAttribute extends Attribute {
    private double size;

    public SizeAttribute(double size) {
        super(Attribute.T_SIZE);
        this.size = size;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }
}
