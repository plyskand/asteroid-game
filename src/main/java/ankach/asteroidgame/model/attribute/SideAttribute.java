package ankach.asteroidgame.model.attribute;

public class SideAttribute extends Attribute {
    private final String side;

    public SideAttribute(String side) {
        super(Attribute.T_SIDE);
        this.side = side;
    }

    public String getSide() {
        return side;
    }
}
