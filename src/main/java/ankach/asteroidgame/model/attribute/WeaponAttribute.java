package ankach.asteroidgame.model.attribute;

import ankach.asteroidgame.entity.Weapon;
import ankach.asteroidgame.model.Timer;

import java.util.ArrayList;
import java.util.List;

public class WeaponAttribute extends Attribute {
    private int chosen = 0;
    private final List<Weapon> weapons = new ArrayList<>();
    private final Timer changeWeaponTimer;

    public WeaponAttribute() {
        super(Attribute.T_WEAPON);
        changeWeaponTimer = new Timer(1000);
    }

    public void changeWeapon() {
        if (changeWeaponTimer.check()) {
            chosen = ((chosen + 1) % weapons.size());
            changeWeaponTimer.reset();
        }
    }

    public String getBulletIdentifier() {
        return weapons.get(chosen).getPath();
    }

    public void addWeapon(Weapon weapon) {
        weapons.add(weapon);
    }

    public Weapon getWeapon() {
        return weapons.get(chosen);
    }

    public List<Weapon> getWeapons (){
        return weapons;
    }

    public int getWeaponAttack() {
       DamageAttribute attribute = (DamageAttribute) weapons.get(chosen).getAttribute(Attribute.T_DAMAGE);
       return attribute.getDamage();
    }

    public int getSpeed() {
        SpeedAttribute attribute = (SpeedAttribute) weapons.get(chosen).getAttribute(Attribute.T_SPEED);
        return attribute.getSpeed();
    }

    public int getChosenCount() {
        return weapons.get(chosen).getCount();
    }

    public int getCountPerShot() {
       return weapons.get(chosen).getCountPerShot();
    }

    public String getDelay() {
       return weapons.get(chosen).getDelay();
    }
}
