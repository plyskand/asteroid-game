package ankach.asteroidgame.model.attribute;

public class SpeedAttribute extends Attribute{

    protected int speed;

    public SpeedAttribute(int speed) {
        super(Attribute.T_SPEED);
        this.speed = speed;
    }

    public SpeedAttribute(int speed, String attrKey) {
        super(attrKey);
        this.speed = speed;
    }

    public int getSpeed() {
        return speed;
    }
}
