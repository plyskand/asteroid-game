package ankach.asteroidgame.model.attribute;

public class DirectionAttribute extends Attribute{
    private final double direction;

    public DirectionAttribute(double direction) {
        super(Attribute.T_DIRECTION);
        this.direction = direction;
    }

    public double getDirection() {
        return direction;
    }
}
