package ankach.asteroidgame.model.attribute;

public class RotationAttribute extends Attribute {
    private double speed;

    public RotationAttribute(double speed) {
        super(Attribute.T_ROTATION);
        this.speed = speed;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }
}
