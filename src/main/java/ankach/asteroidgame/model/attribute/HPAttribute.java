package ankach.asteroidgame.model.attribute;

import ankach.asteroidgame.model.Timer;

public class HPAttribute extends Attribute {
    private int armor;
    private int shield;
    private final int maxArmor;
    private final int maxShield;
    private Timer shieldRegenerateTimer;

    public HPAttribute(int armor, int shield) {
        super(Attribute.T_HP);
        this.armor = armor;
        this.shield = shield;
        this.maxArmor = armor;
        this.maxShield = shield;
        shieldRegenerateTimer = new Timer(1000);
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    public int getShield() {
        return shield;
    }

    public void setShield(int shield) {
        this.shield = shield;
    }

    public int getMaxArmor() {
        return maxArmor;
    }

    public int getMaxShield() {
        return maxShield;
    }

    public boolean isDead() {
        return armor + shield <= 0;
    }

    public void getDamage(int damage) {
        if (shield >= damage) {
            shield -= damage;
        } else {
            damage -= shield;
            shield = 0;
            armor -= damage;
        }
        if (armor < 0) armor = 0;
    }

    public void regenerateShield() {
        if (shield >= maxShield) return;
        if (shieldRegenerateTimer.check()) {
            shieldRegenerateTimer.reset();
            shield += 1;
        }
    }

    public HPAttribute copy() {
        return new HPAttribute(maxArmor, maxShield);
    }
}
