package ankach.asteroidgame.model;

import ankach.asteroidgame.menu.GamePage;
import ankach.asteroidgame.render.Render;
import ankach.asteroidgame.resources.ResourceManager;
import javafx.animation.AnimationTimer;

public class GameCycle
{
    private final GameModel model;
    private final SceneManager manager;
    private final GamePage page;

    public GameCycle(GameModel model, SceneManager manager, GamePage page) {
        this.model = model;
        this.manager = manager;
        this.page = page;
    }

    public void start() {
        Timer timer = new Timer(16);
        new AnimationTimer() {
            public void handle(long currentNanoTime)
            {
                timer.reset();
                if (manager.isGameOver()) {
                    this.stop();
                    page.createAlertWindow(manager.getScore());
                }
                model.update();
                timer.waitToEnd();
            }
        }.start();
    }
}
