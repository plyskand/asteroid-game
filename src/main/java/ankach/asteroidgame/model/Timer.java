package ankach.asteroidgame.model;

public class Timer {
    private long delay;
    private long startTime;

    public Timer(long delay) {
        this.delay = delay;
        this.startTime = System.currentTimeMillis();
    }

    public void reset() {
        this.startTime = System.currentTimeMillis();
    }

    public boolean check() {
        long now = System.currentTimeMillis();
        return startTime + delay < now;
    }

    public long getEndTime() {
        return startTime + delay;
    }

    public void waitToEnd() {
        long now = System.currentTimeMillis();
        long waitingTime = (startTime + delay) - now;
        try {
            if (waitingTime > 0) Thread.sleep(waitingTime);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
