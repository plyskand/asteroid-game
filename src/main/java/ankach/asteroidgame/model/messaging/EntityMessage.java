package ankach.asteroidgame.model.messaging;

import ankach.asteroidgame.entity.Entity;

public class EntityMessage extends Message {
    private final Entity entity;

    public EntityMessage(String type, Entity entity) {
        super(type);
        this.entity = entity;
    }

    public Entity getEntity() {
        return entity;
    }
}
