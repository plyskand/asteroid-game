package ankach.asteroidgame.model.messaging;

import java.util.*;

public class MessageBus {
    private final Map<String, List<Message>> messages = new HashMap<>();

    public void send(Message message) {
        List<Message> list = messages.get(message.getType());
        if (list == null) throw new IllegalStateException("Unknown message type.");
        list.add(message);
    }

    public List<Message> receive(String type) {
        List<Message> list = messages.get(type);
        if (list == null) throw new IllegalStateException("Unknown message type.");
        List<Message> newList = new ArrayList<>(list);
        list.clear();
        return newList;
    }

    public void subscribe(String messageType) {
        messages.put(messageType, new ArrayList<>());
    }
}
