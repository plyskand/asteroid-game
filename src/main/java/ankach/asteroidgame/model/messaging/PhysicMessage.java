package ankach.asteroidgame.model.messaging;

import ankach.asteroidgame.entity.Entity;

public class PhysicMessage extends Message {
    private final Entity first;
    private final Entity second;

    public PhysicMessage(String type, Entity first, Entity second) {
        super(type);
        this.first = first;
        this.second = second;
    }

    public Entity getFirst() {
        return first;
    }

    public Entity getSecond() {
        return second;
    }
}
