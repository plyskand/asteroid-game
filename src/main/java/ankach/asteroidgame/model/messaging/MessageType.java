package ankach.asteroidgame.model.messaging;

public class MessageType {
    public static final String M_MOVING_SYSTEM = "MOVING_SYSTEM";
    public static final String M_UI_SYSTEM = "MOVING_SYSTEM";
    public static final String M_INPUT_SYSTEM = "INPUT_SYSTEM";
    public static final String M_COLLISION_SYSTEM = "COLLISION_SYSTEM";
    public static final String M_SHIELD_SYSTEM = "SHIELD_SYSTEM";
    public static final String M_ROTATE_SYSTEM = "ROTATE_SYSTEM";
    public static final String M_DESTROY_SYSTEM = "DESTROY_SYSTEM";
    public static final String M_FIRE_SYSTEM = "FIRE_SYSTEM";
    public static final String M_PHYSIC_SYSTEM = "PHYSIC_SYSTEM";
    public static final String M_SPAWNER_SYSTEM = "SPAWNER_SYSTEM";
    public static final String M_RENDER_SYSTEM = "RENDER_SYSTEM";
    public static final String M_EXPLOSION_SYSTEM = "EXPLOSION_SYSTEM";
    public static final String M_GAME_PANEL_SYSTEM = "PANEL_SYSTEM";
}
