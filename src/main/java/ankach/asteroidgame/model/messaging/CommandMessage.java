package ankach.asteroidgame.model.messaging;

import ankach.asteroidgame.controller.command.Command;
import ankach.asteroidgame.entity.Entity;

public class CommandMessage extends Message {
    private Entity entity;
    private Command command;

    public CommandMessage(String type, Command command, Entity entity) {
        super(type);
        this.command = command;
        this.entity = entity;
    }

    public Entity getEntity() {
        return entity;
    }

    public Command getCommand() {
        return command;
    }
}
