package ankach.asteroidgame.model;

import ankach.asteroidgame.model.systems.ISystem;

import java.util.ArrayList;
import java.util.List;

public class GameModel {
    private final List<ISystem> systems;

    public GameModel(List<ISystem> systems) {
        this.systems = systems;
    }

    public void update() {
        systems.forEach(ISystem::update);
    }

    public void addSystem(ISystem system) {
        systems.add(system);
    }

    public void removeSystem(ISystem system) {
        systems.remove(system);
    }
}
