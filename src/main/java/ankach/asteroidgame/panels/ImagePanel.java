package ankach.asteroidgame.panels;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;

public class ImagePanel extends Panel {
    private final Image image;

    public ImagePanel(double x, double y, double width, double height, double borderSize, Color fill, Color border, Image image) {
        super(x, y, width, height, borderSize, fill, border);
        this.image = image;
    }

    public Image getImage() {
        return image;
    }
}
