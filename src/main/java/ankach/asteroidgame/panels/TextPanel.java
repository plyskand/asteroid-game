package ankach.asteroidgame.panels;

import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class TextPanel extends Panel {
    private final double padding;
    private final String fixedText;
    private String changedText;
    private final Font font;

    public TextPanel(double x, double y, double width, double height, double borderSize, Color fill, Color border, double padding, String fixedText, Font font) {
        super(x, y, width, height, borderSize, fill, border);
        this.padding = padding;
        this.fixedText = fixedText;
        this.font = font;
    }

    public double getPadding() {
        return padding;
    }

    public String getFixedText() {
        return fixedText;
    }

    public Font getFont() {
        return font;
    }

    public String getChangedText() {
        return changedText;
    }

    public void setChangedText(String changedText) {
        this.changedText = changedText;
    }
}
