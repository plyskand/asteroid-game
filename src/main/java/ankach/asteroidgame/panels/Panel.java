package ankach.asteroidgame.panels;

import javafx.scene.paint.Color;

abstract public class Panel {
    protected Panel parent = null;
    protected double x;
    protected double y;
    protected double width;
    protected double height;
    protected double borderSize;
    protected Color fill;
    protected Color border;

    public Panel(double x, double y, double width, double height, double borderSize, Color fill, Color border) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.borderSize = borderSize;
        this.fill = fill;
        this.border = border;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void setPos(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getBorderSize() {
        return borderSize;
    }

    public void setBorderSize(double border) {
        this.borderSize = border;
    }

    public Panel getParent() {
        return parent;
    }

    public void setParent(Panel parent) {
        this.parent = parent;
    }
}
