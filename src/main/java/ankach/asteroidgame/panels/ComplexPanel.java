package ankach.asteroidgame.panels;

import javafx.scene.paint.Color;

import java.util.HashMap;
import java.util.Map;

public class ComplexPanel extends Panel {
    protected final Map<String, Panel> panels = new HashMap<>();

    public ComplexPanel(double x, double y, double width, double height, double borderSize, Color fill, Color border) {
        super(x, y, width, height, borderSize, fill, border);
    }

    public void addPanel(String key, Panel panel) {
        panels.put(key, panel);
        panel.setParent(this);
    }

    public Map<String, Panel> getPanels() {
        return panels;
    }
}
