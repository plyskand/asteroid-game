package ankach.asteroidgame.panels;

import javafx.scene.paint.Color;

public class ShipPanel extends ComplexPanel {
    public static String ID_ARMOR = "ARMOR";
    public static String ID_SHIELD = "SHIELD";
    public static String ID_SPEED = "SPEED";
    public static String ID_IMAGE = "IMAGE";
    public static String ID_TITLE = "TITLE";

    public ShipPanel(double x, double y, double width, double height, double borderSize, Color fill, Color border) {
        super(x, y, width, height, borderSize, fill, border);
    }

    public void setArmorValue(int value) {
       TextPanel armorPanel = (TextPanel) panels.get(ID_ARMOR);
       armorPanel.setChangedText(String.valueOf(value));
    }

    public void setShieldValue(int value) {
        TextPanel shieldPanel = (TextPanel) panels.get(ID_SHIELD);
        shieldPanel.setChangedText(String.valueOf(value));
    }

    public void setSpeedValue(int value) {
        TextPanel armorPanel = (TextPanel) panels.get(ID_SPEED);
        armorPanel.setChangedText(String.valueOf(value));
    }
}
