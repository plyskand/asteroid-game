package ankach.asteroidgame.resources;

import ankach.asteroidgame.Config;
import ankach.asteroidgame.entity.Score;
import ankach.asteroidgame.entity.ScoreRow;
import ankach.asteroidgame.entity.Sprite;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.image.Image;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Scanner;

public class ResourceManager {
    private final HashMap<String, Image> sprites = new HashMap<>();
    private static ResourceManager manager = null;

    public static ResourceManager getManager() {
       if (manager == null) {
           manager = new ResourceManager();
           manager.init();
       }

       return manager;
    }

    private void init() {
        Config.getExplosions().forEach((path) -> sprites.put(path, new Image(path)));
    }


    public void loadImage(String path, String id, double width, double height) {
        sprites.put(path + id, new Image(path, width, height, false, false));
    }

    public Image getImageById(String id) {
        return sprites.get(id);
    }

    public Sprite createSprite(int width, int height, String path) {
        Image image = sprites.get(path);
        return new Sprite(image, width, height, path);
    }

    public ObservableList<ScoreRow> loadAllScores() {
        ObservableList<ScoreRow> list = FXCollections.observableArrayList();

        File file = new File(Config.SCORE_FILE);
        try {
            Scanner sc = new Scanner(file);
            while (sc.hasNext()) {
                list.add(new ScoreRow(sc.next(), sc.nextInt(), sc.nextInt()));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return list;
    }

    public void saveScore(String username, Score score) {
        try {
            if (username.isBlank()) username = "Unknown_user";
            username = username.replaceAll(" ", "_");
            String line =  "\n" + username + " " + score.getScore() + " " + score.getAmountOfKilledAsteroids();
            Files.write(Paths.get(Config.SCORE_FILE), line.getBytes(), StandardOpenOption.APPEND);
            System.out.println("Score was saved.");
        } catch (IOException e) {
            System.out.println("An error occurred during saving score.");
            e.printStackTrace();
        }
    }

    public static void reset() {
        manager = new ResourceManager();
        manager.init();
    }
}
