package ankach.asteroidgame.controller.command;

import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.enums.Keys;
import ankach.asteroidgame.model.component.Component;
import ankach.asteroidgame.model.component.FireComponent;

public class FireCommand extends Command{
    @Override
    public String getIdentifier() {
        return Keys.KEY_FIRE;
    }

    @Override
    public boolean execute(Component c, Entity e) {
        if (c instanceof FireComponent) {
            ((FireComponent) c).fire(e);
            return true;
        }
        return false;
    }
}
