package ankach.asteroidgame.controller.command;

import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.enums.Keys;
import ankach.asteroidgame.model.component.Component;
import ankach.asteroidgame.model.component.MovingComponent;

public class MoveStraightCommand extends Command {
    @Override
    public String getIdentifier() {
        return Keys.KEY_MOVE_STRAIGHT;
    }

    @Override
    public boolean execute(Component c, Entity e) {
        if (c instanceof MovingComponent) {
            ((MovingComponent) c).moveStraight(e);
            return true;
        }
        return false;
    }
}
