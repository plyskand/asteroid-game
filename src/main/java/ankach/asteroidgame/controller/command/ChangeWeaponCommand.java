package ankach.asteroidgame.controller.command;

import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.enums.Keys;
import ankach.asteroidgame.model.component.ChangeWeaponComponent;
import ankach.asteroidgame.model.component.Component;

public class ChangeWeaponCommand extends Command {
    @Override
    public String getIdentifier() {
        return Keys.KEY_SHIFT;
    }

    @Override
    public boolean execute(Component c, Entity e) {
        if (c instanceof ChangeWeaponComponent) {
            ((ChangeWeaponComponent) c).changeWeapon(e);
            return true;
        }
        return false;
    }
}
