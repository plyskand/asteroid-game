package ankach.asteroidgame.controller.command;

import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.model.component.Component;

/**
 * Empty command, the command does nothing.
 * */
public class NullCommand extends Command {
    @Override
    public String getIdentifier() {
        return null;
    }

    @Override
    public boolean execute(Component c, Entity e) {
        return false;
    }
}
