package ankach.asteroidgame.controller.command;

import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.enums.Keys;
import ankach.asteroidgame.model.component.Component;
import ankach.asteroidgame.model.component.RotationComponent;

public class RotateLeftCommand extends Command {
    @Override
    public String getIdentifier() {
        return Keys.KEY_ROTATE_LEFT;
    }

    @Override
    public boolean execute(Component c, Entity e) {
        if (c instanceof RotationComponent) {
            ((RotationComponent) c).rotateLeft(e);
            return true;
        }
        return false;
    }
}
