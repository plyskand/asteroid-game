package ankach.asteroidgame.controller.command;

import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.model.component.Component;

public abstract class Command {
    /**
     * This method returns a command identifier.
     * */
    public abstract String getIdentifier();

    /**
     * This method make command execution.
     * */
    public abstract boolean execute(Component c, Entity e);
}
