package ankach.asteroidgame.controller.command;

import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.enums.Keys;
import ankach.asteroidgame.model.component.Component;
import ankach.asteroidgame.model.component.MovingComponent;

public class MoveLeftCommand extends Command{

    @Override
    public String getIdentifier() {
        return Keys.KEY_MOVE_LEFT;
    }

    @Override
    public boolean execute(Component c, Entity e) {
        if (c instanceof MovingComponent) {
            ((MovingComponent) c).moveLeft(e);
            return true;
        }
        return false;
    }
}
