package ankach.asteroidgame.controller;

import ankach.asteroidgame.controller.command.*;
import javafx.scene.Scene;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InputController {

    private Scene scene;
    private final List<String> pressedKeysCodes = new ArrayList<>();
    private final Map<String, Command> commandPool = new HashMap<>();

    public InputController() {
        this.addCommand(new MoveStraightCommand());
        this.addCommand(new MoveLeftCommand());
        this.addCommand(new MoveRightCommand());
        this.addCommand(new MoveBackCommand());
        this.addCommand(new RotateLeftCommand());
        this.addCommand(new RotateRightCommand());
        this.addCommand(new FireCommand());
        this.addCommand(new ChangeWeaponCommand());
    }

    private void addCommand(Command command) {
        commandPool.put(command.getIdentifier(), command);
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public void setOnKeyEvent() {
        scene.setOnKeyPressed(
                e -> {
                    String code = e.getCode().toString();
                    this.addKeyPressed(code);
                }
        );
        scene.setOnKeyReleased(
                e -> {
                    String code = e.getCode().toString();
                    this.removeKeyPressed(code);
                }
        );
    }

    public void addKeyPressed(String code) {
        //System.out.println(code);
        if (!pressedKeysCodes.contains(code))
            pressedKeysCodes.add(code);
    }

    public void removeKeyPressed(String code) {
        pressedKeysCodes.remove(code);
    }

    public ArrayList<Command> getCommands() {
        ArrayList<Command> list = new ArrayList<>();
        pressedKeysCodes.forEach((code) -> {
            Command c = commandPool.get(code);
            if (c != null) list.add(c);
        });

        return list;
    }
}
