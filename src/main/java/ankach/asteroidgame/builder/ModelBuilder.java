package ankach.asteroidgame.builder;

import ankach.asteroidgame.model.GameModel;
import ankach.asteroidgame.model.systems.ISystem;

import java.util.ArrayList;
import java.util.List;

public class ModelBuilder {
    private final List<ISystem> systems = new ArrayList<>();

    public ModelBuilder addSystem(ISystem system) {
        systems.add(system);

        return this;
    }

    public GameModel build() {
        return new GameModel(systems);
    }
}
