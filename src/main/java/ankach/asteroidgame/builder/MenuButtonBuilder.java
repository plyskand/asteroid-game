package ankach.asteroidgame.builder;

import javafx.scene.control.Button;
import javafx.scene.text.Font;

public class MenuButtonBuilder {
    private double width;
    private double height;
    private String text;
    private double x;
    private double y;
    private Font font;
    private double shift;

    public void setWidth(double width) {
        this.width = width;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setPos(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void setFont(Font font) {
        this.font = font;
    }

    public void setShift(double shift) {
        this.shift = shift;
    }

    public Button build() {
        Button button = new Button(text);
        button.setLayoutX(x);
        button.setLayoutY(y + shift);
        button.setMinWidth(width);
        button.setMinHeight(height);
        button.setFont(font);
        return button;
    }
}
