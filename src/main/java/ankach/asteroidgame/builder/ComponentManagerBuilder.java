package ankach.asteroidgame.builder;

import ankach.asteroidgame.model.component.Component;
import ankach.asteroidgame.model.component.ComponentManager;

import java.util.HashMap;
import java.util.Map;

public class ComponentManagerBuilder {
    private final Map<String, Component> components = new HashMap<>();

    public ComponentManagerBuilder addComponent(String key, Component component) {
        components.put(key, component);
        return this;
    }

    public ComponentManager build() {
        return new ComponentManager(components);
    }
}
