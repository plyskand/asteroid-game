package ankach.asteroidgame.render;

import ankach.asteroidgame.Config;
import ankach.asteroidgame.entity.Entity;
import ankach.asteroidgame.entity.Sprite;
import ankach.asteroidgame.model.SceneManager;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.transform.Rotate;
import javafx.scene.transform.Scale;

public class Render {
    private final SceneManager sceneManager;
    private GraphicsContext gc;


    public Render() {
        this.sceneManager = SceneManager.getManager();
    }

    public void setGraphicsContext(GraphicsContext gc) {
        this.gc = gc;
    }

    /**
     * Sets the transform for the GraphicsContext to rotate around a pivot point.
     *
     * @param angle the angle of rotation.
     * @param px the x pivot co-ordinate for the rotation (in canvas co-ordinates).
     * @param py the y pivot co-ordinate for the rotation (in canvas co-ordinates).
     */
    public void rotate(double angle, double px, double py) {
        Rotate r = new Rotate(angle, px, py);
        gc.setTransform(r.getMxx(), r.getMyx(), r.getMxy(), r.getMyy(), r.getTx(), r.getTy());
    }

    /**
     * Draws an image on a graphics context.
     *
     * The image is drawn at (tlpx, tlpy) rotated by angle pivoted around the point:
     *   (tlpx + image.getWidth() / 2, tlpy + image.getHeight() / 2)
     *
     */
    public void drawRotatedEntity(Entity entity) {
        Sprite sprite = entity.getSprite();
        Image image = sprite.getImage();
        double angle = entity.getAngle();
        this.drawImage(image, entity.getXPos(), entity.getYPos(), angle);
    }

    public void drawScaledEntity(Entity entity) {
        Sprite sprite = entity.getSprite();
        Image image = sprite.getImage();
        drawImage(image, entity.getXPos(), entity.getYPos(), sprite.getWidth(), sprite.getHeight());
    }

    /**
     * Clears the canvas and draw a background image.
     * */
    public void clear() {
        gc.clearRect(0, 0, Config.getWindowWidth(), Config.getWindowHeight());
        gc.drawImage(sceneManager.getBackground().getImage(), 0, 0);
    }

    public void drawHp(double value, double maxValue, double x, double y, double width, Color fill, Color border) {
        drawRect(x, y, width, 7, border);
        drawRect(x, y, width * (value / maxValue), 7, fill);
    }

    public void drawRect(double x, double y, double width, double height, Color color) {
        gc.setFill(color);
        gc.fillRect(x, y, width, height);
    }

    public void drawText(double x, double y, String value, Color color, int fontSize) {
        gc.setFont(new Font(fontSize));
        gc.setFill(color);
        gc.fillText(value, x, y);
    }

    public void drawImage(Image image, double x, double y, double angle) {
        gc.save(); // saves the current state on stack, including the current transform
        rotate(angle, x + image.getWidth() / 2, y + image.getHeight() / 2);
        gc.drawImage(image, x, y);
        gc.restore(); // back to original state (before rotation)
    }

    public void drawImage(Image image, double x, double y, int width, int height) {
        gc.drawImage(image, x, y, width, height);
    }
}
