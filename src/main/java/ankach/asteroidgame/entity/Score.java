package ankach.asteroidgame.entity;

public class Score {
    private int score = 0;
    private int amountOfKilledAsteroids = 0;

    public Score() {}

    public int getAmountOfKilledAsteroids() {
        return amountOfKilledAsteroids;
    }

    public int getScore() {
        return score;
    }

    public void increaseScore(int score) {
        this.score += score;
        ++this.amountOfKilledAsteroids;
    }
}
