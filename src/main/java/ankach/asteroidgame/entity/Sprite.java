package ankach.asteroidgame.entity;

import javafx.scene.image.Image;

/**
 * This class represents a game sprite, an image of object.
 * */
public class Sprite {
    Image image = null;
    int width;
    int height;
    String path;

    public Sprite(int width, int height, String path) {
        this.width = width;
        this.height = height;
        this.path = path;
    }

    public Sprite(Image image, int width, int height, String path) {
        this.image = image;
        this.width = width;
        this.height = height;
        this.path = path;
    }

    public Image getImage() {
        if (image != null) {           // lazy loading
            return image;
        }
        return loadImage();
    }

    public String getPath() {
        return path;
    }

    private Image loadImage() {
        image = new Image(path, width, height, false, false);
        return image;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
