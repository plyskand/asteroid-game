package ankach.asteroidgame.entity;

import ankach.asteroidgame.model.attribute.Attribute;
import ankach.asteroidgame.model.component.Component;
import javafx.geometry.Point2D;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Entity {
    protected Map<String, Component> components = new HashMap<>();

    protected Map<String, Attribute> attributes = new HashMap<>();

    protected List<Sprite> sprites = new ArrayList<>();

    protected double xPos;
    protected double yPos;
    protected double angle = 0;

    protected double previousXPos;
    protected double previousYPos;
    protected double previousAngle;

    public Entity(Sprite sprite) {
        this.sprites.add(sprite);
    }

    public Sprite getSprite() {
        return sprites.get(0);
    }

    public List<Sprite> getSprites() {
        return sprites;
    }

    public void setPos(double x, double y) {
        this.xPos = x;
        this.yPos = y;
    }

    public Point2D getPos() {
        return new Point2D(xPos, yPos);
    }

    public double getXPos() {
        return xPos;
    }

    public double getYPos() {
        return yPos;
    }

    public void setXPos(double x) {
        this.xPos = x;
    }

    public double getPreviousXPos() {
        return previousXPos;
    }

    public void setPreviousXPos(double previousXPos) {
        this.previousXPos = previousXPos;
    }

    public double getPreviousYPos() {
        return previousYPos;
    }

    public void setPreviousYPos(double previousYPos) {
        this.previousYPos = previousYPos;
    }

    public double getPreviousAngle() {
        return previousAngle;
    }

    public void setPreviousAngle(double previousAngle) {
        this.previousAngle = previousAngle;
    }

    public void restorePreviousPos() {
        this.xPos = previousXPos;
        this.yPos = previousYPos;
        this.angle = previousAngle;
    }

    public void setYPos(double y) {
        this.yPos = y;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public void addAngle(double angle) {
        this.angle = (this.angle + angle) % 360;
    }

    public void addSprite(Sprite sprite) {
        sprites.add(sprite);
    }

    public Component getComponent(String name) {
        return components.get(name);
    }

    public Attribute getAttribute(String name) {
        return attributes.get(name);
    }

    public Map<String, Component> getComponents() {
        return components;
    }

    public Map<String, Attribute> getAttributes() {
        return attributes;
    }

    public void addComponent(Component c) {
        components.put(c.getName(), c);
    }

    public void addAttribute(Attribute a) {
        attributes.put(a.getName(), a);
    }

    public double getCenterXPos() {
        return xPos + getSprite().width / 2.0;
    }

    public double getCenterYPos() {
        return yPos + getSprite().height / 2.0;
    }

    public double getCircleRadius() {
        return getSprite().width / 2.0;
    }
}
