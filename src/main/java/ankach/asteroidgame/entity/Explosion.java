package ankach.asteroidgame.entity;

public class Explosion extends Entity {
    private final int frameToLive;
    private int frameLive = 0;

    public Explosion(Sprite sprite, int frameToLive) {
        super(sprite);
        this.frameToLive = frameToLive;
    }

    @Override
    public Sprite getSprite() {
        int size = sprites.size();
        int framesForOneSprite = frameToLive/(size) + 1;
        int index = frameLive++ / framesForOneSprite;
        return sprites.get(index);
    }

    public boolean isAlive() {
        return frameLive < frameToLive - 1;
    }
}
