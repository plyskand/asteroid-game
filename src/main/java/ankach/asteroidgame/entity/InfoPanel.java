package ankach.asteroidgame.entity;

import ankach.asteroidgame.Config;
import javafx.geometry.Rectangle2D;

public class InfoPanel {
    private final Rectangle2D panel;
    private final Rectangle2D weaponPanel;
    private final Rectangle2D shipPanel;
    private final Rectangle2D scorePanel;
    private final String weaponText = "Current weapon";
    private final String shipText = "Your ship";
    private final String attackText = "Attack: ";
    private final String armorText = "Armor: ";
    private final String shieldText = "Shield: ";
    private final String countText = "Count: ";
    private final String scoreText = "Score";
    private final String speedText = "Speed: ";


    public InfoPanel () {
        double x = 0;
        double y = Config.getWindowHeight() - 210;
        double width = 770;
        double height = 210;
        panel = new Rectangle2D(x, y, width, height);
        shipPanel = new Rectangle2D(x + 5, y + 5, 200, height - 10);
        weaponPanel = new Rectangle2D(x + 210, y + 5, 200, height - 10);
        scorePanel = new Rectangle2D(x + 415, y + 5, 350, height - 10);
    }

    public Rectangle2D getPanel() {
        return panel;
    }

    public Rectangle2D getWeaponPanel() {
        return weaponPanel;
    }

    public Rectangle2D getShipPanel() {
        return shipPanel;
    }

    public Rectangle2D getScorePanel() {
        return scorePanel;
    }

    public String getWeaponText() {
        return weaponText;
    }

    public String getAttackText() {
        return attackText;
    }

    public String getArmorText() {
        return armorText;
    }

    public String getShieldText() {
        return shieldText;
    }

    public String getCountText() {
        return countText;
    }

    public String getScoreText() {
        return scoreText;
    }

    public String getShipText() {
        return shipText;
    }

    public String getSpeedText() {
        return speedText;
    }
}
