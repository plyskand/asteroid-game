package ankach.asteroidgame.entity;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class ScoreRow {
    private final SimpleStringProperty username;
    private final SimpleIntegerProperty score;
    private final SimpleIntegerProperty asteroids;

    public ScoreRow(String username, int score, int asteroids) {
        this.username = new SimpleStringProperty(username);
        this.score = new SimpleIntegerProperty(score);
        this.asteroids = new SimpleIntegerProperty(asteroids);
    }

    public String getUsername() {
        return username.get();
    }

    public SimpleStringProperty usernameProperty() {
        return username;
    }

    public int getScore() {
        return score.get();
    }

    public SimpleIntegerProperty scoreProperty() {
        return score;
    }

    public int getAsteroids() {
        return asteroids.get();
    }

    public SimpleIntegerProperty asteroidsProperty() {
        return asteroids;
    }
}
