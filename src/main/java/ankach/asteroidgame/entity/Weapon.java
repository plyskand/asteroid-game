package ankach.asteroidgame.entity;

import ankach.asteroidgame.model.Timer;

/**
 * This class represents a ship weapon.
 * Each weapon uses different count of items per shot.
 * Each weapon has reloading time, so player can not shoot more times during some period of time that delay is.
 * */
public class Weapon extends Entity {
    private int count;
    private int countPerShot = 1;
    private final Timer timer;

    public Weapon(Sprite sprite, int count, long delay) {
        super(sprite);
        this.count = count;
        timer = new Timer(delay);
    }

    public Weapon(Sprite sprite, int count, int countPerShot, long delay) {
        super(sprite);
        this.count = count;
        this.countPerShot = countPerShot;
        timer = new Timer(delay);
    }

    /**
     * The method gets a path to the image for this weapon.
     * @return String - a path
     * */
    public String getPath() {
        return getSprite().getPath();
    }

    public int getCount() {
        return count;
    }

    public void decCount() {
        --count;
    }

    /**
     * Method checks if a player can shoot from current weapon.
     * */
    public boolean canFire() {
        if (timer.check() && count > 0) {
            timer.reset();
            return true;
        }
        return false;
    }

    /**
     * Method creates a simple timer.
     * @return String formatted time in seconds.
     * */
    public String getDelay() {
        long endTime = timer.getEndTime();
        long now = System.currentTimeMillis();
        long diff = endTime - now;
        if (diff < 0) return "0 s";
        long elapsedSeconds = diff / 1000;
        long secondsDisplay = elapsedSeconds % 60 + 1;

        return secondsDisplay + " s";
    }


    public int getCountPerShot() {
        return countPerShot;
    }
}
