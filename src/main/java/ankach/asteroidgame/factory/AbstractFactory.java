package ankach.asteroidgame.factory;

import ankach.asteroidgame.Config;
import ankach.asteroidgame.entity.Asteroid;
import ankach.asteroidgame.entity.Ship;
import ankach.asteroidgame.entity.Sprite;
import ankach.asteroidgame.entity.Weapon;
import ankach.asteroidgame.enums.Sides;
import ankach.asteroidgame.model.attribute.*;
import ankach.asteroidgame.model.component.Component;
import ankach.asteroidgame.model.component.ComponentManager;
import ankach.asteroidgame.spawner.AsteroidSpawner;

/**
 * This class is responsible for game object creation.
 * */
public abstract class AbstractFactory {
    protected final ComponentManager componentManager;

    public AbstractFactory() {
        this.componentManager = ComponentManager.getManager();
    }

    /**
     * Method generates a random number in range.
     * @param min - min random number
     * @param max - max random number
     * @return int - a number between min and max.
     * */
    public int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    /**
     * Method creates a base skeleton of an asteroid. It can be redefined in child classes.
     * */
    public Asteroid createAsteroid() {
        int randomNumber = getRandomNumber(0, 9);
        int size = getRandomNumber(Config.getAsteroidMinSize(), Config.ASTEROID_MAX_SIZE);
        Sprite sprite = new Sprite(size, size, Config.getAsteroids().get(randomNumber));
        Asteroid asteroid = new Asteroid(sprite);
        asteroid.addComponent(componentManager.getComponent(Component.NAME_ROTATION));
        asteroid.addComponent(componentManager.getComponent(Component.NAME_ONE_DIRECTION_MOVING));
        asteroid.addComponent(componentManager.getComponent(Component.NAME_RENDER));
        double x = -getRandomNumber(0, Config.WINDOW_WIDTH);
        double y = -getRandomNumber(0, Config.WINDOW_HEIGHT);
        asteroid.setPos(x, y);

        return asteroid;
    }

    /**
     * Method creates a base skeleton of a ship. It can be redefined in child classes.
     * */
    public Ship createShip() {
        Ship ship = new Ship(
                new Sprite(
                        Config.getShipWidth(),
                        Config.getShipHeight(),
                        Config.getShips().get(0)
                )
        );
        ship.addComponent(componentManager.getComponent(Component.NAME_MOVING));
        ship.addComponent(componentManager.getComponent(Component.NAME_CHANGE_WEAPON));
        ship.addComponent(componentManager.getComponent(Component.NAME_ROTATION));
        ship.addComponent(componentManager.getComponent(Component.NAME_RENDER_SHIP));
        ship.addComponent(componentManager.getComponent(Component.NAME_FIRE));
        ship.setPos(500, 500);
        ship.addAngle(-90);

        return ship;
    }


    /**
     * Method creates and adds a base weapon to a ship.
     * @param ship - user ship.
     * */
    public void addWeapons(Ship ship) {
        Weapon weapon = createWeapon(new Sprite(30, 30, Config.getBulletIdentifiers().get(3)), 100000, 1, 100);
        weapon.addAttribute(new HPAttribute(1, 0));
        weapon.addAttribute(new DamageAttribute(5));
        weapon.addAttribute(new SpeedAttribute(15));
        weapon.addAttribute(new SideAttribute(Sides.SIDE_PLAYER));
        WeaponAttribute attribute = new WeaponAttribute();
        attribute.addWeapon(weapon);
        ship.addAttribute(attribute);
    }

    /**
     * Method creates a base skeleton of a weapon.
     * @param sprite - an object represents an image.
     * @param count - count of bullets for current weapon.
     * @param countPerShot - count of bullets weapon use per one shot.
     * @param delay - time in milliseconds between two shots.
     * @return Weapon - weapon skeleton.
     * */
    protected Weapon createWeapon(Sprite sprite, int count, int countPerShot, long delay) {
        Weapon weapon = new Weapon(sprite, count, countPerShot, delay);
        weapon.addComponent(componentManager.getComponent(Component.NAME_ONE_DIRECTION_MOVING));
        weapon.addComponent(componentManager.getComponent(Component.NAME_ROTATION));
        weapon.addComponent(componentManager.getComponent(Component.NAME_RENDER));

        return weapon;
    }

    public AsteroidSpawner createSpawner() {
        return new AsteroidSpawner(this, 1000, 25);
    }
}
