package ankach.asteroidgame.factory;

import ankach.asteroidgame.entity.Bullet;
import ankach.asteroidgame.entity.Sprite;
import ankach.asteroidgame.entity.Weapon;
import ankach.asteroidgame.model.attribute.*;
import ankach.asteroidgame.model.component.Component;

public class BulletFactory {
    public BulletFactory() {}

    public Bullet create(Weapon weapon) {
        weapon.decCount();
        Bullet bullet = new Bullet(new Sprite(
                30,
                30,
                weapon.getPath()
        ));

        bullet.addComponent(weapon.getComponent(Component.NAME_MOVING));
        bullet.addComponent(weapon.getComponent(Component.NAME_ROTATION));
        bullet.addComponent(weapon.getComponent(Component.NAME_RENDER));
        bullet.addAttribute(weapon.getAttribute(Attribute.T_SPEED));
        bullet.addAttribute(weapon.getAttribute(Attribute.T_SIDE));
        bullet.addAttribute(weapon.getAttribute(Attribute.T_DAMAGE));
        HPAttribute hp = ((HPAttribute) weapon.getAttribute(Attribute.T_HP)).copy();
        bullet.addAttribute(hp);
        return bullet;
    }
}
