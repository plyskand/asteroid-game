package ankach.asteroidgame.factory;

import ankach.asteroidgame.panels.ComplexPanel;
import ankach.asteroidgame.panels.ShipPanel;
import ankach.asteroidgame.panels.TextPanel;
import ankach.asteroidgame.resources.ResourceManager;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/** TODO if time left */
public class PanelFactory {
    private ResourceManager manager;

    public PanelFactory() {
        manager = ResourceManager.getManager();
    }

    public ComplexPanel createShipPanel(double x, double y) {
        double size = 200;
        double fontSize = 20;
        Color color = Color.WHITE;
        Font font = new Font("Times New Roman", fontSize);
        double border = 5;
        double width = size + border * 2;
        double height = size + border * 2;

        ComplexPanel ship = new ShipPanel(x, y, width, height, border, Color.DARKBLUE, Color.BLUE);
        TextPanel title = new TextPanel(
                10, 30, size, height,0, color, null,
                0, "Your ship", new Font("Times New Roman", fontSize + 5));


        TextPanel armor = new TextPanel(
                10, 40, size, fontSize, 0, color, null,
                0, "Armor: ", font
        );
        TextPanel shield = new TextPanel(
                10, 40, size, fontSize, 0, color, null,
                0, "Shield: ", font
        );
        TextPanel speed = new TextPanel(
                10, 40, size, fontSize, 0, color, null,
                0, "Speed: ", font
        );
        ship.addPanel(ShipPanel.ID_TITLE, title);
        ship.addPanel(ShipPanel.ID_SHIELD, shield);
        ship.addPanel(ShipPanel.ID_ARMOR, armor);
        ship.addPanel(ShipPanel.ID_SPEED, speed);

        return ship;
    }

    public ComplexPanel createWeaponPanel(double x, double y) {
        double size = 200;
        double fontSize = 20;
        Color color = Color.WHITE;
        Font font = new Font("Times New Roman", fontSize);
        double border = 5;
        double width = size + border * 2;
        double height = size + border * 2;

        ComplexPanel ship = new ComplexPanel(x, y, width, height, border, Color.DARKBLUE, Color.BLUE);
        TextPanel title = new TextPanel(
                10, 30, width, height,0, color, null,
                0, "Current weapon", new Font("Times New Roman", fontSize + 5));
        TextPanel attack = new TextPanel(
                x + 10, y + 60, width, fontSize, 0, color, null,
                0, "Attack: ", font
        );
        TextPanel count = new TextPanel(
                x + 10, y + 40, width, fontSize, 0, color, null,
                0, "Count: ", font
        );
        TextPanel speed = new TextPanel(
                x + 10, y + 40, width, fontSize, 0, color, null,
                0, "Speed: ", font
        );


        return ship;
    }
}
