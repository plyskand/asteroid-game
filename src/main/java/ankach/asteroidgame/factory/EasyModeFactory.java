package ankach.asteroidgame.factory;

import ankach.asteroidgame.Config;
import ankach.asteroidgame.entity.Asteroid;
import ankach.asteroidgame.entity.Ship;
import ankach.asteroidgame.entity.Sprite;
import ankach.asteroidgame.entity.Weapon;
import ankach.asteroidgame.enums.Sides;
import ankach.asteroidgame.model.attribute.*;

public class EasyModeFactory extends AbstractFactory{
    @Override
    public Asteroid createAsteroid() {
        Asteroid asteroid = super.createAsteroid();
        int size = getRandomNumber(Config.getAsteroidMinSize(), Config.ASTEROID_MAX_SIZE);
        asteroid.addAttribute(new RotationAttribute(getRandomNumber(1, Config.ASTEROID_MAX_ROTATE_SPEED)));
        asteroid.addAttribute(new SpeedAttribute(getRandomNumber(1, Config.ASTEROID_MAX_SPEED)));
        asteroid.addAttribute(new SizeAttribute(size));
        asteroid.addAttribute(new SideAttribute(Sides.SIDE_ASTEROIDS));
        asteroid.addAttribute(new DamageAttribute(size/15));
        asteroid.addAttribute(new HPAttribute(getRandomNumber(Config.ASTEROID_MIN_HP - Config.ASTEROID_MIN_HP/2, Config.ASTEROID_MAX_HP - Config.ASTEROID_MAX_HP/2), 0));
        asteroid.addAttribute(new DirectionAttribute(getRandomNumber(1, 360)));

        return asteroid;
    }

    @Override
    public Ship createShip() {
        Ship ship = super.createShip();
        // setting ship attributes
        ship.addAttribute(new SpeedAttribute(Config.getShipSpeed() + 10) );
        ship.addAttribute(new SpeedAttribute(Config.getShipSpeed() - 7, Attribute.T_SPEED_JINK));
        ship.addAttribute(new RotationAttribute(Config.getShipRotateSpeed()));
        ship.addAttribute(new SideAttribute(Sides.SIDE_PLAYER));
        ship.addAttribute(new DamageAttribute(1));
        ship.addAttribute(new HPAttribute(500, 50));
        addWeapons(ship);

        return ship;
    }

    /**
     * Method creates user weapons for easy mode and adds them to ship.
     * */
    @Override
    public void addWeapons(Ship ship) {
        super.addWeapons(ship);
        WeaponAttribute att = (WeaponAttribute) ship.getAttribute(Attribute.T_WEAPON);
        Weapon weapon = createWeapon(new Sprite(30, 30, Config.getBulletIdentifiers().get(0)), 500, 1, 400);
        weapon.addAttribute(new HPAttribute(2, 0));
        weapon.addAttribute(new DamageAttribute(15));
        weapon.addAttribute(new SpeedAttribute(6));
        weapon.addAttribute(new SideAttribute(Sides.SIDE_PLAYER));
        att.addWeapon(weapon);
        weapon = createWeapon(new Sprite(30, 30, Config.getBulletIdentifiers().get(3)), 10000, 4, 100);
        weapon.addAttribute(new HPAttribute(1, 0));
        weapon.addAttribute(new DamageAttribute(5));
        weapon.addAttribute(new SpeedAttribute(15));
        weapon.addAttribute(new SideAttribute(Sides.SIDE_PLAYER));
        att.addWeapon(weapon);
        weapon = createWeapon(new Sprite(30, 30, Config.getBulletIdentifiers().get(4)), 5000, 100, 5000);
        weapon.addAttribute(new HPAttribute(3, 0));
        weapon.addAttribute(new DamageAttribute(10));
        weapon.addAttribute(new SpeedAttribute(12));
        weapon.addAttribute(new SideAttribute(Sides.SIDE_PLAYER));
        att.addWeapon(weapon);
        weapon = createWeapon(new Sprite(30, 30, Config.getBulletIdentifiers().get(4)), 10000, 6, 50);
        weapon.addAttribute(new HPAttribute(3, 0));
        weapon.addAttribute(new DamageAttribute(10));
        weapon.addAttribute(new SpeedAttribute(15));
        weapon.addAttribute(new SideAttribute(Sides.SIDE_PLAYER));
        att.addWeapon(weapon);
    }
}
