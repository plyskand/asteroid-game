package ankach.asteroidgame.menu;

import ankach.asteroidgame.Config;
import ankach.asteroidgame.builder.MenuButtonBuilder;
import ankach.asteroidgame.builder.ModelBuilder;
import ankach.asteroidgame.controller.InputController;
import ankach.asteroidgame.entity.Score;
import ankach.asteroidgame.entity.Sprite;
import ankach.asteroidgame.factory.AbstractFactory;
import ankach.asteroidgame.factory.EasyModeFactory;
import ankach.asteroidgame.model.GameCycle;
import ankach.asteroidgame.model.GameModel;
import ankach.asteroidgame.model.SceneManager;
import ankach.asteroidgame.model.component.ComponentManager;
import ankach.asteroidgame.model.messaging.MessageBus;
import ankach.asteroidgame.model.systems.*;
import ankach.asteroidgame.render.Render;
import ankach.asteroidgame.resources.ResourceManager;
import ankach.asteroidgame.spawner.AsteroidSpawner;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class GamePage {
    private final Stage window;
    private Stage alertWindow = null;
    private Group root;
    private InputController controller;
    private GameCycle gameCycle;
    private final Menu menu;

    public GamePage(Stage window, Menu menu) {
        this.window = window;
        this.menu = menu;
    }

    public void init() {
        this.window.setMinWidth(Config.WINDOW_WIDTH);
        this.window.setMinHeight(Config.WINDOW_HEIGHT + 20);
        this.window.setResizable(false);
        SceneManager.reset();
        ResourceManager.reset();
        ComponentManager.reset();
        SceneManager sceneManager = SceneManager.getManager();
        AbstractFactory factory = new EasyModeFactory();
        AsteroidSpawner spawner = factory.createSpawner();
        ModelBuilder builder = new ModelBuilder();
        MessageBus bus = new MessageBus();
        Render render = new Render();
        controller = new InputController();

        // configuring scene manager
        sceneManager.setBackground(new Sprite(Config.getWindowWidth(), Config.getWindowHeight(), Config.getBackgroundPath()));
        sceneManager.setShip(factory.createShip());

        // configuring java fx nodes
        Canvas canvas = new Canvas(Config.getWindowWidth(), Config.getWindowHeight());
        GraphicsContext gc = canvas.getGraphicsContext2D();
        root = new Group();
        root.getChildren().add(canvas);
        render.setGraphicsContext(gc);

        // configuring game model
        GameModel model = builder
                .addSystem(new InputSystem(bus, controller))
                .addSystem(new UISystem(bus))
                .addSystem(new SpawnerSystem(bus, spawner))
                .addSystem(new MovingSystem(bus))
                .addSystem(new RotateSystem(bus))
                .addSystem(new FireSystem(bus))
                .addSystem(new CollisionSystem(bus))
                .addSystem(new PhysicalSystem(bus))
                .addSystem(new DestroySystem(bus))
                .addSystem(new ExplosionSystem(bus))
                .addSystem(new RenderSystem(bus, render))
                .addSystem(new ShieldRegenerationSystem(bus))
                .build();

        gameCycle = new GameCycle(model, sceneManager, this);
    }

    public void start() {
        window.getScene().setRoot(root);
        controller.setScene(window.getScene());
        controller.setOnKeyEvent();
        gameCycle.start();
    }

    public void createAlertWindow(Score score) {
        alertWindow = new Stage();
        alertWindow.setMinWidth(500);
        alertWindow.setMinHeight(300);
        Group group = new Group();
        Scene scene = new Scene(group);
        Rectangle rec = new Rectangle(0, 0, 500, 300);
        rec.setFill(Color.RED);
        Label labelDestroyed = new Label("Game over!!!");
        Label labelEnterName = new Label("Type your name for saving score");
        labelDestroyed.setFont(Font.font("Courier New", FontWeight.BOLD, 30));
        labelEnterName.setFont(Font.font("Courier New", FontWeight.BOLD, 22));
        labelDestroyed.setLayoutX(150);
        labelDestroyed.setLayoutY(20);
        labelEnterName.setLayoutX(55);
        labelEnterName.setLayoutY(117);
        TextField textField = new TextField();
        textField.setMinWidth(200);
        textField.setLayoutX(150);
        textField.setLayoutY(150);
        group.getChildren().addAll(rec, labelDestroyed, labelEnterName, textField);
        addButtonsForAlertWindow(group, textField, score);

        alertWindow.setOnCloseRequest(e -> menu.backToMainMenu());
        alertWindow.setScene(scene);
        alertWindow.show();
    }

    private void addButtonsForAlertWindow(Group group, TextField textField, Score score) {
        MenuButtonBuilder builder = new MenuButtonBuilder();
        builder.setFont(Font.font("Courier New", FontWeight.BOLD, 20));
        builder.setWidth(150);
        builder.setHeight(30);
        builder.setShift(200);
        builder.setText("Save");
        builder.setPos(90, 0);

        Button saveButton = builder.build();
        saveButton.setOnAction(e -> {
            ResourceManager resourceManager = ResourceManager.getManager();
            resourceManager.saveScore(textField.getText(), score);
            menu.backToMainMenu();
            alertWindow.close();
        });
        builder.setText("Close");
        builder.setPos(250, 0);
        Button cancelButton = builder.build();
        cancelButton.setOnAction(e -> {
            menu.backToMainMenu();
            alertWindow.close();
        });
        group.getChildren().addAll(saveButton, cancelButton);
    }
}
