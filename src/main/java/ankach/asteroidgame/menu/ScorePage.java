package ankach.asteroidgame.menu;

import ankach.asteroidgame.Config;
import ankach.asteroidgame.entity.ScoreRow;
import ankach.asteroidgame.resources.ResourceManager;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class ScorePage {
    private final Stage window;
    private final ImageView background;
    private final Button backToMainMenu;
    private Group group;

    public ScorePage(Stage window, Menu menu) {
        this.window = window;
        background = new ImageView(new Image(Config.getBackgroundPath()));
        background.setFitWidth(500);
        background.setFitHeight(500);

        backToMainMenu = new Button("Back to main menu");
        backToMainMenu.setLayoutX(50);
        backToMainMenu.setLayoutY(50);
        backToMainMenu.setOnAction(e -> menu.backToMainMenu());
    }

    public void init() {
        TableView table = createTable();
        final VBox vbox = new VBox();
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().addAll(backToMainMenu, table);
        group = new Group(background, vbox);
    }

    public void start() {
        window.getScene().setRoot(group);
    }

    private TableView createTable(){
        TableView table = new TableView();
        ResourceManager manager = ResourceManager.getManager();
        ObservableList<ScoreRow> scores = manager.loadAllScores();

        TableColumn<ScoreRow, String> usernameCol = new TableColumn<ScoreRow, String>("Username");
        usernameCol.setMinWidth(100);
        usernameCol.setCellValueFactory(p -> new SimpleStringProperty(p.getValue().getUsername()));
        TableColumn<ScoreRow, Integer> scoreCol = new TableColumn<ScoreRow, Integer>("Score");
        scoreCol.setCellValueFactory(p -> new SimpleIntegerProperty(p.getValue().getScore()).asObject());
        scoreCol.setMinWidth(100);
        TableColumn<ScoreRow, Integer> asteroidCountCol = new TableColumn<ScoreRow, Integer>("Amount of destroyed asteroids");
        asteroidCountCol.setCellValueFactory(p -> new SimpleIntegerProperty(p.getValue().getAsteroids()).asObject());
        asteroidCountCol.setMinWidth(275);

        table.getColumns().addAll(usernameCol, scoreCol, asteroidCountCol);
        table.setItems(scores);
        return table;
    }
}
