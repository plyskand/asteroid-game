package ankach.asteroidgame.menu;

import ankach.asteroidgame.Config;
import ankach.asteroidgame.builder.MenuButtonBuilder;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;


public class Menu {

    private final GamePage gamePage;
    private final ScorePage scorePage;
    private final Stage window;
    private final Group mainMenuGroup;

    public Menu(Stage window) {
        gamePage = new GamePage(window, this);
        scorePage = new ScorePage(window, this);
        this.window = window;
        this.window.setMinWidth(500);
        this.window.setMinHeight(500);
        this.window.setX(200);
        this.window.setY(0);
        this.window.setTitle(Config.getWindowTitle());
        this.window.setResizable(false);

        ImageView background = new ImageView(new Image(Config.getBackgroundPath()));
        background.setFitWidth(500);
        background.setFitHeight(500);

        MenuButtonBuilder builder = new MenuButtonBuilder();
        builder.setShift(120);
        builder.setFont(Font.font("Courier New", FontWeight.BOLD, 25));
        builder.setWidth(200);
        builder.setHeight(50);

        builder.setText("Start game");
        builder.setPos(150, 20);
        Button startButton = builder.build();
        startButton.setOnAction(e -> startGame());

        builder.setText("Records");
        builder.setPos(150, 80);
        Button recordsButton = builder.build();
        recordsButton.setOnAction(e -> showRecords());

        builder.setText("Exit");
        builder.setPos(150, 140);
        Button closeButton = builder.build();
        closeButton.setOnAction(e -> window.close());

        mainMenuGroup = new Group();
        mainMenuGroup.getChildren().addAll(background, startButton, recordsButton, closeButton);
        window.setScene(new Scene(mainMenuGroup));
    }

    public void start() {
        window.show();
    }

    private void startGame() {
        gamePage.init();
        gamePage.start();
    }

    private void showRecords() {
        scorePage.init();
        scorePage.start();
    }

    public void backToMainMenu() {
        window.setScene(new Scene(mainMenuGroup));
        this.window.setResizable(true);
        this.window.setMinWidth(500);
        this.window.setMinHeight(500);
        this.window.setWidth(500);
        this.window.setHeight(500);
    }
}
