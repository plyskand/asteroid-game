package ankach.asteroidgame.spawner;

import ankach.asteroidgame.entity.Asteroid;
import ankach.asteroidgame.factory.AbstractFactory;
import ankach.asteroidgame.model.Timer;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class AsteroidSpawner {
    private final int maxCount;
    private Timer timer;
    private final AbstractFactory factory;

    public AsteroidSpawner(AbstractFactory factory, long delay, int count) {
        this.factory = factory;
        this.maxCount = count;
        timer = new Timer(delay);
    }

    public Asteroid createAsteroid(int actualCount) {
        if (actualCount + 1 > maxCount) {
            return null;
        }
        if (!timer.check()) {
            return null;
        }

        timer.reset();
        return factory.createAsteroid();
    }

    public List<Asteroid> generateAsteroids(int actualCount, int a) {
        if (actualCount >= maxCount)
            return new ArrayList<>();
        int n = factory.getRandomNumber(1, maxCount - actualCount);
        return IntStream.range(0, n).mapToObj(i -> factory.createAsteroid()).collect(Collectors.toList());
    }

    public int getMaxCount() {
        return maxCount;
    }


}
