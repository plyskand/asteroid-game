package ankach.asteroidgame.spawner;

import ankach.asteroidgame.entity.Bullet;
import ankach.asteroidgame.entity.Weapon;
import ankach.asteroidgame.factory.BulletFactory;
import ankach.asteroidgame.model.Timer;

public class BulletSpawner {

    private final BulletFactory factory;
    private Timer timer;

    public BulletSpawner() {
        this.factory = new BulletFactory();
        this.timer = new Timer(100);
    }

    public Bullet createBullet(Weapon weapon) {
        return factory.create(weapon);
    }
}
