package ankach.asteroidgame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Config {
    public static final int WINDOW_WIDTH = 1700;
    public static final int WINDOW_HEIGHT = 1080;
    public static final int PANEL_WIDTH = 770;
    public static final int PANEL_HEIGHT = 210;
    public static final String WINDOW_TITLE = "Asteroids";
    public static final String SCORE_FILE = "src/main/resources/scores.txt";

    private static final List<String> BACKGROUNDS = new ArrayList<>(Arrays.asList(
            "file:src/main/resources/backgrounds/Background1.png",
            "file:src/main/resources/backgrounds/Background2.png",
            "file:src/main/resources/backgrounds/Background3.png",
            "file:src/main/resources/backgrounds/Background4.png"
    ));
    private static final List<String> ASTEROIDS = new ArrayList<>(Arrays.asList(
            "file:src/main/resources/asteroids/rock_type_0.png",
            "file:src/main/resources/asteroids/rock_type_1.png",
            "file:src/main/resources/asteroids/rock_type_2.png",
            "file:src/main/resources/asteroids/rock_type_3.png",
            "file:src/main/resources/asteroids/rock_type_4.png",
            "file:src/main/resources/asteroids/rock_type_5.png",
            "file:src/main/resources/asteroids/rock_type_6.png",
            "file:src/main/resources/asteroids/rock_type_7.png",
            "file:src/main/resources/asteroids/rock_type_8.png",
            "file:src/main/resources/asteroids/rock_type_9.png"
    ));

    private static final List<String> SHIPS = new ArrayList<>(List.of(
            "file:src/main/resources/ships/ship1.png"
    ));

    private static final List<String> BULLETS = new ArrayList<>(List.of(
            "file:src/main/resources/bullets/rocket_1.png",
            "file:src/main/resources/bullets/rocket_2.png",
            "file:src/main/resources/bullets/rocket_3.png",
            "file:src/main/resources/bullets/bullet.png",
            "file:src/main/resources/bullets/Plasma.png"
    ));

    private static final List<String> EXPLOSIONS = new ArrayList<>(List.of(
            "file:src/main/resources/explosion/Explosion_0.png",
            "file:src/main/resources/explosion/Explosion_1.png",
            "file:src/main/resources/explosion/Explosion_2.png",
            "file:src/main/resources/explosion/Explosion_3.png",
            "file:src/main/resources/explosion/Explosion_4.png",
            "file:src/main/resources/explosion/Explosion_5.png",
            "file:src/main/resources/explosion/Explosion_6.png",
            "file:src/main/resources/explosion/Explosion_7.png",
            "file:src/main/resources/explosion/Explosion_8.png"
    ));

    private static final int SHIP_WIDTH = 100;
    private static final int SHIP_HEIGHT = 100;
    private static final int SHIP_SPEED = 10;
    private static final int SHIP_ROTATE_SPEED = 5;

    public static final int ASTEROID_MIN_SIZE = 25;
    public static final int ASTEROID_MAX_SIZE = 150;
    public static final int ASTEROID_MIN_HP = 10;
    public static final int ASTEROID_MAX_HP = 100;
    public static final int ASTEROID_MAX_SPEED = 10;
    public static final int ASTEROID_MAX_ROTATE_SPEED = 5;

    public static int getWindowWidth() {
        return WINDOW_WIDTH;
    }

    public static int getWindowHeight() {
        return WINDOW_HEIGHT;
    }

    public static String getWindowTitle() {
        return WINDOW_TITLE;
    }

    public static String getBackgroundPath() {
        return BACKGROUNDS.get(2);
    }

    public static List<String> getAsteroids() {
        return ASTEROIDS;
    }

    public static List<String> getShips() {
        return SHIPS;
    }

    public static int getAsteroidMinSize() {
        return ASTEROID_MIN_SIZE;
    }

    public static int getAsteroidMaxSize() {
        return ASTEROID_MAX_SIZE;
    }

    public static int getAsteroidMaxSpeed() {
        return ASTEROID_MAX_SPEED;
    }

    public static int getAsteroidMaxRotateSpeed() {
        return ASTEROID_MAX_ROTATE_SPEED;
    }

    public static int getShipWidth() {
        return SHIP_WIDTH;
    }

    public static int getShipHeight() {
        return SHIP_HEIGHT;
    }

    public static int getShipSpeed() {
        return SHIP_SPEED;
    }

    public static int getShipRotateSpeed() {
        return SHIP_ROTATE_SPEED;
    }

    public static List<String> getBulletIdentifiers() {
        return BULLETS;
    }

    public static List<String> getExplosions(){
        return EXPLOSIONS;
    }
}
