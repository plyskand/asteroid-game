package ankach.asteroidgame;

import ankach.asteroidgame.menu.Menu;
import javafx.application.Application;
import javafx.stage.Stage;

public class Launcher extends Application {
    @Override
    public void start(Stage stage) {
        Menu menu = new Menu(stage);
        menu.start();
    }
    public static void main(String[] args) {
        launch();
    }
}
