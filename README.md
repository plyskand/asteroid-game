## This is a simple game which was written using javafx and different programming patters.

### About game

#### Objects:
    - Asteroids, 
    - Ships,
    - Bullets.

#### Actions:
    - flying in space,
    - destroying asteroids,
    - switching weapons,
    - scoring,

#### Technical mechanic:
    - the player have to destroy asteroids
    - asteroids can destroy the player
    - player can changing weapons during a game
    - player ship has shiels which can regenerate
    - no borders on the map

#### Keyboard
    UP - move streight
    DOWN - move back
    RIGHT - move right
    LEFT - move left
    Z - rotate left
    X - rotate right
    SHIFT - change weapon
    SPACE - fire

### Used patterns:
    1) Command pattern:
        package: ankach.asteroidgame.controller.command
    2) ECS - Entity Component System pattern:
        package: ankach.asteroidgame.model
    3) Messaging:
        package: ankach.asteroidgame.model.messaging
    4) Decorator pattern, classes:
        - ShipRenderComponent
        - OneDirectionMovingComponent
    5) Singleton pattern, classes:
        - ResourceManager
        - ComponentManager
        - SceneManager
        - all components are something like singletons, or like a services. They exist at the system only as one instance.
    6) State pattern:
        class: WeaponAttribute - firering according to a state the weapon is. Change weapon botton will change the state.
    7) Flyweight: 
        class: Entity consists a reference to components which are a single objects in a system.
    8) AbstractFactory pattern: 
        package: ankach.asteroidgame.factory - modes
    9) Object pool pattern:
        class: InputController - commands
    10) Visitor pattern:
        classes: Command and Component implements something like a visitor pattern.
    11) Lazy Loading:
        class: Sprite - the image is lazy loaded.
    12) Builder Pattern:
        - class: ModelBuilder
        - class: ComponentManagerBuilder